-- SPDX-FileCopyrightText: 2022 David Hurka <doxydoxy@mailbox.org>
--
-- SPDX-License-Identifier: MIT OR LGPL-2.1-or-later

local S = minetest.get_translator("advtrains_attachment_offset_patch");

local player_configs = minetest.get_mod_storage();

--! Returns whether player @p playername has this feature enabled.
--! Returns the default configuration value,
--! if no configuration for @p playername is present.
function advtrains_attachment_offset_patch.is_enabled_for_player(playername)
    local config_key = "advtrains_attachment_offset_patch:player_config_" .. playername;
    local config_value = player_configs:get_string(config_key);

    if config_value == "on" then
        return true;
    elseif config_value == "off" then
        return false;
    else
        return minetest.settings:get_bool("advtrains_attachment_offset_patch_default", true);
    end
end

--! Handler function for the /advtrains_attachment_offset_patch_config command.
--!
--! @param playername Name of the player who executes the command.
--! @param arguments Space separated string, which was passed to the command.
function advtrains_attachment_offset_patch.chat_command_handler(playername, arguments)
    arguments = string.split(arguments, " ");

    if not (#arguments == 1 or #arguments == 2) then
        return false;
    end

    local referenced_player = playername;

    if #arguments == 2 then
        referenced_player = arguments[2];

        if referenced_player == playername then
            -- Players may configure themselves.
        elseif not minetest.check_player_privs(playername, "server") then
            -- Players may configure others only with server privilege.
            minetest.chat_send_player(playername, S("You need the server privilege to change settings of other players."));
            -- Minetest bug 12435: Returning true is silly, but necessary.
            return true;
        end
    end

    local config_key = "advtrains_attachment_offset_patch:player_config_" .. referenced_player;

    local new_setting = string.lower(arguments[1]);

    if new_setting == "default" then
        player_configs:set_string(config_key, "");
    elseif new_setting == "on" then
        player_configs:set_string(config_key, "on");
    elseif new_setting == "off" then
        player_configs:set_string(config_key, "off");
    else
        return false;
    end

    return true;
end

minetest.register_chatcommand("advtrains_attachment_offset_patch_config", {
        params = "on|off|default [<player>]";
        description = S([[
Enables or disables the workaround for Minetest bug 10101 for advtrains wagons that are set up for advtrains_attachment_offset_patch.

The workaround can have unintended consequences, like a view that wobbles around or is frozen at (0, 0, 0).
Therefore, this workaround can be enabled or disabled by individual players using this command.

If you omit <player>, the command references you.
]]);
        func = advtrains_attachment_offset_patch.chat_command_handler;
    });
