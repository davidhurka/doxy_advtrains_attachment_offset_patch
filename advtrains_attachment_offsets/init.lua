-- SPDX-FileCopyrightText: 2022 David Hurka <doxydoxy@mailbox.org>
--
-- SPDX-License-Identifier: MIT OR LGPL-2.1-or-later

--! Function @c vector.new.
local V = vector.new;

--! Function @c vector.copy, but using @c vector.new for old Minetest versions.
local VV = vector.copy or V;

--! Returns vector @p a scaled by vector @p b.
local function scale(a, b)
    return V(a.x * b.x, a.y * b.y, a.z * b.z);
end

--! Makes an advtrains wagon definition called @p wagon_name use the
--! advtrains_attachment_offset_patch API, if it exists.
--!
--! Eye offsets are reset for all seats.
local function fix_wagon(wagon_name)
    local wagon = advtrains.wagon_prototypes[wagon_name];
    if not wagon then
        return;
    end

    if rawget(wagon, "get_on") or rawget(wagon, "get_off") then
        -- Do not disimprove mods, which already use the workaround,
        -- or implement special code.
        -- This will hopefully happen after this mod is released.
        return;
    end

    for _, seat in ipairs(wagon.seats) do
        seat.view_offset = V(0, 0, 0);
    end

    advtrains_attachment_offset_patch.setup_advtrains_wagon(wagon);

    return wagon
end

--! Returns the sign of @p a, -1 or 1.
local function sign(a)
    if a < 0 then
        return -1;
    else
        return 1;
    end
end

local w;

-- basic_trains: industrial
w = fix_wagon("advtrains:engine_industrial");
if w then
    w.seats[2].attach_offset = scale(VV(w.seats[1].attach_offset), V(-1, 1, 1));
end

w = fix_wagon("advtrains:engine_industrial_big");
if w then
    w.seats[2].attach_offset = scale(VV(w.seats[1].attach_offset), V(1, 1, -1));
end

-- basic_trains: japan
w = fix_wagon("advtrains:engine_japan");
if w then
    w.seats[1].attach_offset.y = 1;
    w.seats[1].attach_offset.z = 11;

    for i = 2, 5 do
        w.seats[i].attach_offset.y = 0;
    end
end

w = fix_wagon("advtrains:wagon_japan");
if w then
    for i = 1, 4 do
        w.seats[i].attach_offset.y = 0;
    end
end

-- basic_trains: steam
w = fix_wagon("advtrains:newlocomotive");
if w then
    for i = 1, 2 do
        w.seats[i].attach_offset.y = 3;
        w.seats[i].attach_offset.z = -11;
    end
end

w = fix_wagon("advtrains:detailed_steam_engine");
if w then
    for i = 1, 2 do
        w.seats[i].attach_offset.x = sign(w.seats[i].attach_offset.x) * 9;
        w.seats[i].attach_offset.y = -1;
    end
end

w = fix_wagon("advtrains:wagon_default");
if w then
    for i = 1, 4 do
        w.seats[i].attach_offset.x = (i % 2) == 0 and 3 or 1;
        w.seats[i].attach_offset.y = -1;
        w.seats[i].view_offset.y = -1;
        w.seats[i].advtrains_attachment_offset_patch_attach_rotation = V(0, ((i % 2) == 0 and 0 or 180), 0);
    end

    w.seats[2].attach_offset.z = 3;
    w.seats[3].attach_offset.z = -3;
end

-- basic_trains: subway
w = fix_wagon("advtrains:subway_wagon");
if w then
    for i = 2, 5 do
        w.seats[i].attach_offset.y = 0;
        w.seats[i].attach_offset.z = sign(w.seats[i].attach_offset.z) * 5;
        w.seats[i].advtrains_attachment_offset_patch_attach_rotation = V(0, ((i % 2) == 0 and 90 or -90), 0);
    end

    w.seats[1].attach_offset.y = 2;
    w.seats[1].attach_offset.z = 12;
end

-- dlxtrains: locomotives
fix_wagon("dlxtrains_diesel_locomotives:locomotive_type1");

-- dlxtrains: support
w = fix_wagon("dlxtrains_support_wagons:escort_type1");
if w then
    for i = 2, 3 do
        w.seats[i].advtrains_attachment_offset_patch_attach_rotation = V(0, 180, 0);
    end
end

w = fix_wagon("dlxtrains_support_wagons:caboose_type1");
if w then
    for i = 1, 2 do
        w.seats[i].attach_offset.x = sign(w.seats[i].attach_offset.x) * 4.5;
        w.seats[i].view_offset.y = -2;
    end

    w.seats[1].advtrains_attachment_offset_patch_attach_rotation = V(0, 90, 0);
    w.seats[2].advtrains_attachment_offset_patch_attach_rotation = V(0, 270, 0);

    w.seats[4].attach_offset = scale(w.seats[3].attach_offset, V(-1, 1, -1));
    w.seats[4].advtrains_attachment_offset_patch_attach_rotation = V(0, 180, 0);
end

-- moretrains: basic
fix_wagon("advtrains:moretrains_diesel_german");

w = fix_wagon("advtrains:moretrains_railroad_car");
if w then
    w.seats[6].attach_offset.y = -2;

    for i = 1, 2 do
        w.seats[i].advtrains_attachment_offset_patch_attach_rotation = V(0, 180, 0);
    end

    for i = 3, 4 do
        w.seats[i].attach_offset.z = 3;
    end
end

w = fix_wagon("advtrains:moretrains_silberling");
if w then
    w.seats[1].advtrains_attachment_offset_patch_attach_rotation = V(0, 270, 0);
    w.seats[1].attach_offset.y = 0;
    w.seats[1].attach_offset.z = 10;
    w.seats[1].view_offset.y = -2;

    w.seats[3].attach_offset.z = 3;
    w.seats[3].advtrains_attachment_offset_patch_attach_rotation = V(0, 180, 0);

    w.seats[4].advtrains_attachment_offset_patch_attach_rotation = V(0, 180, 0);

    w.seats[6].advtrains_attachment_offset_patch_attach_rotation = V(0, 90, 0);
    w.seats[6].attach_offset.y = 0;
    w.seats[6].attach_offset.z = -10;
    w.seats[6].view_offset.y = -2;
end

fix_wagon("advtrains:moretrains_silberling_dining");

w = fix_wagon("advtrains:moretrains_silberling_train");
if w then
    w.seats[1].attach_offset.x = 2;

    w.seats[4].attach_offset.z = 3;
    w.seats[4].advtrains_attachment_offset_patch_attach_rotation = V(0, 180, 0);

    w.seats[5].advtrains_attachment_offset_patch_attach_rotation = V(0, 180, 0);

    w.seats[7].attach_offset.y = -2;
end

-- moretrains: japan
w = fix_wagon("advtrains:moretrains_engine_japan");
if w then
    w.seats[1].view_offset.y = 1.5;

    for i = 4, 5 do
        w.seats[i].advtrains_attachment_offset_patch_attach_rotation = V(0, 180, 0);
    end
end

w = fix_wagon("advtrains:moretrains_wagon_japan");
if w then
    w.seats[1].view_offset.y = 1.5;

    for i = 3, 4 do
        w.seats[i].attach_offset.z = -4;
    end

    for i = 5, 6 do
        w.seats[i].advtrains_attachment_offset_patch_attach_rotation = V(0, 180, 0);
    end
end

-- moretrains: nightline
w = fix_wagon("advtrains:moretrains_nightline_couchette");
if w then
    for i = 1, 4 do
        w.seats[i].attach_offset.x = -3;
    end
end

w = fix_wagon("advtrains:moretrains_nightline_seat_car");
if w then
    for i = 1, 4 do
        w.seats[i].attach_offset.x = -3;
    end
end

-- moretrains: steam
w = fix_wagon("advtrains:moretrains_steam_train");
if w then
    for i = 1, 2 do
        w.seats[i].view_offset.y = 10;
    end
end

-- jre231
w = fix_wagon("advtrains:KuHa_E231");
if w then
    w.seats[1].attach_offset.y = 0;

    for i = 2, 5 do
        w.seats[i].attach_offset.z = sign(w.seats[i].attach_offset.z) * -18;
        w.seats[i].attach_offset.y = 0;
        w.seats[i].advtrains_attachment_offset_patch_attach_rotation = V(0, (i % 2) == 0 and 90 or 270, 0);
    end
end

w = fix_wagon("advtrains:MoHa_E230");
if w then
    for i = 1, 6 do
        w.seats[i].attach_offset.z = sign(w.seats[i].attach_offset.z) * 18;
        w.seats[i].attach_offset.y = 0;
        w.seats[i].advtrains_attachment_offset_patch_attach_rotation = V(0, (i % 2) == 0 and 270 or 90, 0);
    end
end

w = fix_wagon("advtrains:MoHa_E231");
if w then
    for i = 1, 6 do
        w.seats[i].attach_offset.z = sign(w.seats[i].attach_offset.z) * 18;
        w.seats[i].attach_offset.y = 0;
        w.seats[i].advtrains_attachment_offset_patch_attach_rotation = V(0, (i % 2) == 0 and 270 or 90, 0);
    end
end

w = fix_wagon("advtrains:SaHa_E231");
if w then
    for i = 1, 6 do
        w.seats[i].attach_offset.z = sign(w.seats[i].attach_offset.z) * 18;
        w.seats[i].attach_offset.y = 0;
        w.seats[i].advtrains_attachment_offset_patch_attach_rotation = V(0, (i % 2) == 0 and 270 or 90, 0);
    end
end

-- tfls7
w = fix_wagon("advtrains:under_s7dm");
if w then
    w.seats[1].attach_offset.x = -6;
    w.seats[1].attach_offset.y = 0;

    for i = 2, 5 do
        w.seats[i].attach_offset.z = sign(w.seats[i].attach_offset.z) * -18;
        w.seats[i].attach_offset.y = 0;
        w.seats[i].advtrains_attachment_offset_patch_attach_rotation = V(0, (i % 2) == 0 and 90 or 270, 0);
    end
end

w = fix_wagon("advtrains:under_s7ndm");
if w then
    for i = 1, 6 do
        w.seats[i].attach_offset.z = sign(w.seats[i].attach_offset.z) * 18;
        w.seats[i].attach_offset.y = 0;
        w.seats[i].advtrains_attachment_offset_patch_attach_rotation = V(0, (i % 2) == 0 and 270 or 90, 0);
    end
end

-- jpntlr600
for _, color in ipairs({ "blue", "green", "red", "yellow" }) do
    w = fix_wagon("advtrains:Tlr600_" .. color .. "_front");
    if w then
        for i = 2, 3 do
            w.seats[i].advtrains_attachment_offset_patch_attach_rotation = V(0, 180, 0);
        end

        for i = 4, 5 do
            w.seats[i].attach_offset.z = -10;
            w.seats[i].advtrains_attachment_offset_patch_attach_rotation = V(0, 90, 0);
        end
    end

    w = fix_wagon("advtrains:Tlr600_" .. color .. "_back");
    if w then
        for i = 1, 2 do
            w.seats[i].attach_offset.z = 10;
            w.seats[i].advtrains_attachment_offset_patch_attach_rotation = V(0, 270, 0);
        end

        for i = 3, 4 do
            w.seats[i].attach_offset.z = -7;
        end
    end
end

-- somemoretrains: tram
w = fix_wagon("advtrains:tram");
if w then
    for i = 2, 5 do
        w.seats[i].advtrains_attachment_offset_patch_attach_rotation = V(0, (i % 2) == 0 and 90 or 270, 0);
    end
end

-- rocket
w = fix_wagon("advtrains:rocket");
if w then
    w.seats[1].attach_offset.y = 0;
end

-- bboe_1080
w = fix_wagon("advtrains:engine_BBOE_1080");
if w then
    w.seats[1].attach_offset.x = 3;
    w.seats[1].attach_offset.y = 0;

    for i = 2, 5 do
        w.seats[i].attach_offset.y = 0;
        w.seats[i].advtrains_attachment_offset_patch_attach_rotation = V(0, (i % 2) == 0 and 90 or 270, 0);
    end

    w.seats[2].attach_offset.z = 5;
    w.seats[3].attach_offset.z = 5;
    w.seats[4].attach_offset.z = -3;
    w.seats[5].attach_offset.z = -3;
end

-- advtrains_construction_train
w = fix_wagon("advtrains:construction_train");
if w then
    for i = 2, 5 do
        w.seats[i].attach_offset.y = 0;
        w.seats[i].attach_offset.z = sign(w.seats[i].attach_offset.z) * 5;
        w.seats[i].advtrains_attachment_offset_patch_attach_rotation = V(0, ((i % 2) == 0 and 90 or -90), 0);
    end

    w.seats[1].attach_offset.y = 2;
    w.seats[1].attach_offset.z = 12;
end

-- db_160
w = fix_wagon("advtrains:engine_DB_160");
if w then
    w.seats[1].attach_offset = V(4, 1, -3);
    w.seats[2].attach_offset = V(-4, 1, -3);
    w.seats[3] = nil;
    w.seats[4] = nil;
    w.seats[5] = nil;
end

-- advtrains_freight_train
fix_wagon("advtrains:diesel_lokomotive");

-- advtrains_granite
w = fix_wagon("advtrains:granite_locomotive");
if w then
    for i = 1, 2 do
        w.seats[i].attach_offset.y = 2;
    end
end

-- advtrains_railbus
w = fix_wagon("advtrains:engine_railbus");
if w then
    for i = 2, 5 do
        w.seats[i].advtrains_attachment_offset_patch_attach_rotation = V(0, ((i % 2) == 0 and 90 or -90), 0);
    end

    w.seats[1].attach_offset = V(0, 4, 18);
    w.seats[2].attach_offset = V(-4, 2, 4);
    w.seats[3].attach_offset = V(4, 2, 4);
    w.seats[4].attach_offset = V(-4, 2, -4);
    w.seats[5].attach_offset = V(4, 2, -4);
end

-- advtrains_subway_ny
w = fix_wagon("advtrains:NY_lokomotive");
if w then
    w.seats[1].attach_offset = V(-3, -3, 2);
    w.seats[2].attach_offset = V(-3, -3, -6);
    w.seats[3].attach_offset = V(3, -3, -6);
    w.seats[4].attach_offset = V(-3, -3, -12);
    w.seats[5].attach_offset = V(3, -3, -12);
end

w = fix_wagon("advtrains:ny_wagon");
if w then
    for i = 1, 6 do
        w.seats[i].attach_offset.y = -3;
    end
end

-- advtrains_train_orient_express
w = fix_wagon("advtrains:engine_electronic");
if w then
    w.seats[1].attach_offset = V(0, -3, 10);

    w.seats[2].attach_offset = V(0, -3, -10);
    w.seats[2].advtrains_attachment_offset_patch_attach_rotation = V(0, 180, 0);
end

w = fix_wagon("advtrains:wagon_compartment");
if w then
    w.seats[1].attach_offset = V(2, -2, 12);
    w.seats[2].attach_offset = V(2, -2, 4);
    w.seats[3].attach_offset = V(2, -2, -4);
    w.seats[4].attach_offset = V(2, -2, -12);
end

w = fix_wagon("advtrains:wagon_sleep");
if w then
    w.seats[1].attach_offset = V(2, -2, 12);
    w.seats[2].attach_offset = V(2, -2, 4);
    w.seats[3].attach_offset = V(2, -2, -4);
    w.seats[4].attach_offset = V(2, -2, -12);
end

-- advtrains_train_zugspitzbahn
w = fix_wagon("advtrains:engine_zugspitzbahn");
if w then
    w.seats[1].attach_offset = V(3, 0, 17);

    w.seats[2].attach_offset = V(3, 0, -17);
    w.seats[2].advtrains_attachment_offset_patch_attach_rotation = V(0, 180, 0);
end

w = fix_wagon("advtrains:wagon_zugspitzbahn");
if w then
    w.seats[1].attach_offset = V(3, -1, 8);
    w.seats[1].advtrains_attachment_offset_patch_attach_rotation = V(0, 180, 0);

    w.seats[2].attach_offset = V(-3, -1, 8);
    w.seats[2].advtrains_attachment_offset_patch_attach_rotation = V(0, 180, 0);

    w.seats[3].attach_offset = V(3, -1, -5);
    w.seats[4].attach_offset = V(-3, -1, -5);
end

-- advtrains_transib
w = fix_wagon("advtrains:engine_transib");
if w then
    w.seats[1].attach_offset = V(5, 0, 11);
    w.seats[2].attach_offset = V(-5, 0, 11);
    w.seats[3] = nil;
    w.seats[4] = nil;
    w.seats[5] = nil;
end

-- disco_trains
for _, color in ipairs({ "blue", "red", "green", "black", "white", "gray" }) do
    w = fix_wagon("advtrains:subway_wagon_" .. color);
    if w then
        for i = 2, 5 do
            w.seats[i].attach_offset.y = 0;
            w.seats[i].attach_offset.z = sign(w.seats[i].attach_offset.z) * 5;
            w.seats[i].advtrains_attachment_offset_patch_attach_rotation = V(0, ((i % 2) == 0 and 90 or -90), 0);
        end

        w.seats[1].attach_offset.y = 2;
        w.seats[1].attach_offset.z = 12;
    end
end
