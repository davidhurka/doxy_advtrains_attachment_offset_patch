<!--
SPDX-FileCopyrightText: 2022 David Hurka <doxydoxy@mailbox.org>

SPDX-License-Identifier: MIT OR CC-BY-SA-4.0
-->

# advtrains_attachment_offsets

Implements the workaround for [Minetest bug 10101](https://github.com/minetest/minetest/issues/10101) for various train mods, by making them use the API of `advtrains_attachment_offsets_patch`.
