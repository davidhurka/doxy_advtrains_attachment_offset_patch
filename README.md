<!--
SPDX-FileCopyrightText: 2022 David Hurka <doxydoxy@mailbox.org>

SPDX-License-Identifier: MIT OR CC-BY-SA-4.0
-->

# advtrains_attachment_offset_patch

This modpack implements the workaround for [Minetest bug 10101](https://github.com/minetest/minetest/issues/10101), with API for `advtrains` wagons.

## Mods in this modpack

### advtrains_attachment_offset_patch

Adds “attachment dummy” objects to seats in `advtrains` wagons, so the Minetest client will set the eye position correctly.
Mods which add train models to `advtrains` have to use the API to make their wagons use the workaround.

### advtrains_attachment_offsets

Makes various train mods use the `advtrains_attachment_offset_patch` API.

## Issues

* When I join the game, I am not correctly reattached to some wagons.
    - This is a bug in the `on_joinplayer` callback handler of `advtrains`, and is not related to this mod.

## Installation

This mod is intended to be installed from Minetest’s own content manager, [ContentDB](https://content.minetest.net/doxygen_spammer/advtrains_attachment_offset_patch).

You can also clone the repository to your `mods` folder.
You will need to disable the “LICENSES” and “screenshots” mods, if your Minetest can not figure out that these aren’t mods.

You can also use this `bash` command from the root directoy of the repository:

```bash
git archive --format tar HEAD | tar --extract --one-top-level=advtrains_attachment_offset_patch --directory=path/to/minetest/mods/
```

(Using GNU tar.)

## License

The mod is licensed as CC-BY-1.0 (media) and MIT (code).

Screenshots depict artwork from other Minetest mods, and are licensed as CC-BY-SA-4.0.

## Contributing

The source code is hosted at <https://invent.kde.org/davidhurka/doxy_advtrains_attachment_offset_patch>.
Problems should be reported at <https://invent.kde.org/davidhurka/doxy_advtrains_attachment_offset_patch/issues>.
